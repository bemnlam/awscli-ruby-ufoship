FROM atlassian/pipelines-awscli:latest AS base

RUN apk update && apk upgrade && apk --update add \
    ruby ruby-irb ruby-rake ruby-io-console ruby-bigdecimal ruby-json ruby-bundler \
    libstdc++ tzdata bash ca-certificates \
    &&  echo 'gem: --no-document' > /etc/gemrc

CMD ["irb"]

RUN gem install ufo
RUN gem list ufo
RUN aws --version

COPY scripts/configure-aws.sh ./
RUN ls -al ./

RUN cat configure-aws.sh